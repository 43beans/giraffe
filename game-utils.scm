;;; game-utils
;; utilities for little text games

(import (only (chicken random) pseudo-random-integer))

;;; Dice rolls
(define (roll n)
  ;;; Roll a dice with N sides.
  ;; Return a random number 1--N.
  (+ 1 (pseudo-random-integer n)))

;;; Incrementing/decrementing numbers
(define-syntax inc!                     ; (inc! x)
  ;;; Increment X by N or 1 and return X.
  (syntax-rules ()
    ((_ x) (begin (set! x (+ x 1)) x))
    ((_ x n) (begin (set! x (+ x n)) x))))

(define-syntax dec!                     ; (dec! x)
  ;;; Decrement X by N or 1 and return X.
  (syntax-rules ()
    ((_ x) (begin (set! x (inc! x -1)) x))
    ((_ x n) (begin (set! x (inc! x (- n))) x))))

(define-syntax dec0!                    ; (dec0! x)
  ;;; Decrement X by N or 1.
  ;; If X is negative, set it to 0.
  ;; Return X.
  (syntax-rules ()
    ((_ x) (begin (dec! x)
                  (when (< x 0) (set! x 0))
                  x))
    ((_ x n) (begin (dec! x n)
                    (when (< x 0) (set! x 0))
                    x))))

;;; User interface
;; (define (char-line c n)
;;   (string-append (make-string n c)
;;                  (make-string (- 10 n) #\-)))
(define (string-lines . strs)
  ;;; Append STRS into one string separated by newlines.
  ;; If one of STRS is a list, all of its items will be appended into one line
  ;; in the final string.
  (apply string-append
         (append (intersperse
                  (map (lambda (s)
                         (if (list? s)
                             (apply string-append (map ->string s))
                             (->string s)))
                       strs)
                  "\n")
                 '("\n"))))

(define (clear-screen)
  ;;; Clear the terminal screen.
  (display #\escape)
  (display "[2J")
  (newline))

(define (displayln . strs)
  ;;; Display STRS using `string-lines'.
  (display (apply string-lines strs)))

(define (display-columns a b)
  ;;; Display strings A and B in columns.
  (let loop ((a (string-split a "\n"))
             (a-max 0)
             (b (string-split b "\n")))
    (cond
     ((and (null? a) (null? b)) 'done)
     ((null? a)
      (display (make-string a-max #\space))
      (display #\tab)
      (display (car b))
      (newline)
      (loop '() a-max (cdr b)))
     ((null? b)
      (display (car a))
      (newline)
      (loop (cdr a)
            (if (> (string-length (car a)) a-max)
                (string-length (car a))
                a-max)
            '()))
     (else
      (display (car a))
      (display #\tab)
      (display (car b))
      (newline)
      (loop (cdr a)
            (if (> (string-length (car a)) a-max)
                (string-length (car a))
                a-max)
            (cdr b))))))

;;; Interaction
(define (ask question #!optional prompt max-lines input-end)
  ;;; Display QUESTION and wait for user input.
  ;; PROMPT (default: "> ") will show the user where to type.  Input ends either
  ;; after MAX-LINES are read (default: read forever), or if a line matches
  ;; INPUT-END (default: ".", like ed).
  (let ((prompt (or prompt "> "))
        (input-end (or input-end ".")))
    (display question)
    (newline)
    (display prompt)
    (let loop ((answer '())
               (line (read-line)))
      (set! answer (cons line answer))
      (cond
       ((and max-lines (>= (length answer) max-lines))
        (reverse answer))
       ((string=? line input-end)
        (reverse (cdr answer)))
       (else (loop answer
                   (begin (display prompt)
                          (read-line))))))))
 ;; module game-utils ends here
