;;; there is a giraffe who wants to gte the best leaves but his neck is too
;;; short
;; a one-player rpg by dozens (https://dozens.itch.io/giraffe)
;; programmed by acdw
;; a 43beans joint

(import (chicken io)
        (only (chicken file) create-temporary-file)
        (chicken random)
        utf8)

(include "game-utils")

;;; About
(define about
#<<end
This is a role playing game. Here's what you need to know:

1. You are a giraffe
2. You want to get the best leaves
3. They are high up in the tree
4. Your neck is too short and you cannot reach them.
5. Sorrow!  Hunger!  Yearning!
end
)

;;; Stats (also known as global variables, or sometimes parameters)
;; Stats are named in ALL_CAPS_AND_SNAKE_CASE.  Some are defined elsewhere, but
;; most are defined here
(define OUTPUT (create-temporary-file ".giraffe")) ; TODO: save to a "real" file
(define PROMPT "`/\"> ")
(define INPUT_END ".")

;;; Create a giraffe
;; 1. name your giraffe
(define NAME (ask "Hi there giraffe!  What's your name?" PROMPT 1))
;; 2. choose ONE adjective to describe your giraffe
(define ADJECTIVE (ask "What's ONE adjective that describes you?" PROMPT 1))
;; 3. you have one stat called GIRAFFE. it starts at 3.
(define GIRAFFE 3)

;;; Doing stuff
(define (display-stats)
  (define neck-desc
    (cond
     ((< GIRAFFE 3) "but your neck is far too short to reach.")
     ((< 3 GIRAFFE 5) "but your neck is too short to reach.")
     ((< 5 GIRAFFE 6) "and your neck is nearly long enough to reach.")
     ((>= GIRAFFE 6) "and you can reach them with your long neck!")))
  (display-columns
   (string-lines `("You are a " ,ADJECTIVE " giraffe named " ,NAME ".")
                 "You want the tallest, sweetest leaves,"
                 neck-desc) ; TODO: CONTINUE
   (string-lines )))
;; 1. describe the situation
;; 2. describe what you want to do.
;; 3. you do it.
;; 4. describe the outcome.

;;; Doing stuff (with dice!)
;; if an action is risky or the outcome uncertain, start with the procedure
;; above, but then roll a 6-sided die.

;; if you are attempting to do something giraffe-like, bestial, or animal-like,
;; you must roll /under/ GIRAFFE.

;; if you are attempting to do something that's more human like, that relies on
;; cunning, deception, detailed planning, or abstract reasoning, roll /over/
;; GIRAFFE.

;; if you can apply your ADJECTIVE to the situation, roll twice and take the
;; best result.

;;; Results
;; if you succeed in your roll, describe the positive outcome.  if you fail,
;; describe your failure.  alternatively, describe a partial success, or a
;; success at cost.

;;; Special rolls
;; 1. GIRAFFE?!!: if you roll your GIRAFFE number exactly, you have a profound
;; epiphany.  a moment of insight into the ways of the universe.  you learn one
;; secret or one thing you wouldn't ordinarily be able to know.

;; 2. EXCEPTIONAL ROLLS: when highs are high, and lows are low:
;; - if you're trying to roll low and roll a 1, or if you're trying to roll high
;; and roll a 6, that's an exceptional success!  things go great and you get a
;; bonus.
;; - same is true for failures: if you are trying to roll low and roll a 6, or
;; if you're trying to roll high and roll a 1, that is an exceptional failure.
;; things don't go as planned, plus there's an unforeseen consequence, and/or
;; you gain a lingering condition such as #confused or #sprainedankle.

;; 3. EVOLVING: whenever you have an Exceptional Roll, your GIRAFFE number
;; changes.
;; - if you exceptionally succeed while attempting to do something giraffe-like,
;; add one to GIRAFFE.  if you exceptionally fail while attempting something
;; giraffe-like, subtract one from GIRAFFE.
;; - do the opposite for attempting human-like stuff: exceptional success ->
;; subtract 1; exceptional failure -> add one.

;;; End
;; If your GIRAFFE number reaches 6, you evolve into the platonic ideal of a
;; giraffe.  your neck is so long.  you can easily reach all the best leaves.

;; if GIRAFFE reqches 1, you devolve into a miserable human.  you never get the
;; best leaves.  instead you have to go get a job and work for the rest of your
;; life.

;;; Acknowledgements and credits
;; game by dozens
;; code by acdw
;; thanks to Lasers and Feelings and the tilde.town #towngame gang

